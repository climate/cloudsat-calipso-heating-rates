# Take the yearly output of make_binned_heating.py and postprocess: proper time and height coordinates, set metadata, save into one netcdf file with all years
#
# on IMG JET call with /jetfs/home/avoigt/micromamba/envs/cloudsat-calipso/bin/python3.12  postprocess_binned_heating.py

import xarray as xr
import numpy as np

import sys
sys.path.append("/jetfs/home/avoigt/cloudsat-calipso-heating-rates/core/")
import core as core

import warnings
warnings.filterwarnings("ignore")


# derive characteristic height as the mean height from a sample granule
file="/jetfs/shared-data/CLOUDSAT/Data/2B-FLXHR-LIDAR.P2_R05/2010/150/2010150002219_21737_CS_2B-FLXHR-LIDAR_GRANULE_P2_R05_E03_F00.hdf"
height = core.read_height(file)["HGT"].mean("y")


ds_list = list()
for year in ["2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017"]:
    ds = xr.open_dataset("/jetfs/scratch/avoigt/CLOUDSAT/2B-FLXHR-LIDAR.P2_R05.heatingrates_binned."+year+".nc", chunks="auto")
    time = np.arange(year+"-01-03", year+"-12-31", 5, dtype='datetime64[D]')
    ds = ds.rename({"pentad": "time"}).assign_coords({"time": time})
    ds = ds.assign_coords({"height": height.values})
    ds = ds.assign_attrs({"Data": "Atmospheric radiative heating rates based on 2B-FLXHR-LIDAR.P2_R05"})
    ds = ds.assign_attrs({"Data producer": "Aiko Voigt, Dept of Meteorology and Geophysics, University of Vienna"})
    ds = ds.assign_attrs({"Time": "Data generated on April 5, 2024"})
    ds = ds.assign_attrs({"Software": "https://gitlab.phaidra.org/climate/cloudsat-calipso-heating-rates"})
    text = "Geographical variations in the actual height of the height bins are neglected for simplicity. " \
         "Data on the same height bins are averaged together. "\
         "The height of the heights bins is defined as the mean height along a sample granule."
    ds = ds.assign_attrs({"Note on height coordinate": text})
    ds_list.append(ds)
    del ds

# merge into one dataset and save to netcdf
ds_merged = xr.concat(ds_list, "time")
ds_merged.to_netcdf("/jetfs/scratch/avoigt/CLOUDSAT/2B-FLXHR-LIDAR.P2_R05.heatingrates_binned.2006-2017.nc")

