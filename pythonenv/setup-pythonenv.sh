# create environment on IMG Jet using micromamba

module load micromamba
micromamba create -n cloudsat-calipso
micromamba install -c conda-forge -n cloudsat-calipso xarray \ rioxarray \ numpy \ matplotlib \ cartopy \ pyhdf \ netcdf4
micromamba install -c conda-forge -n cloudsat-calipso dask \ zarr \ numba \ astropy

# create Jupyter kernel and make available
micromamba install -c conda-forge -n cloudsat-calipso ipykernel
~/micromamba/envs/cloudsat-calipso/bin/python3 -m ipykernel install --user --name=cloudsat-calipso

# export evironment setup to env.txt
micromamba env export -p ~/micromamba/envs/cloudsat-calipso/ > env.txt
