#!/bin/bash
#SBATCH --job-name=sbatch_jet_submit.sh
#SBATCH --output=sbatch_jet_submit.log
#SBATCH --ntasks=40
#SBATCH --time=12:00:00

cd /jetfs/home/avoigt/cloudsat-calipso-heating-rates
/jetfs/home/avoigt/micromamba/envs/cloudsat-calipso/bin/python3.12  make_binned_heating_R04.py
