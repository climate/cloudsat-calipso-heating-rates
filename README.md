# Cloudsat Calipso heating rates

Collection of scripts and figures for cloud-radiative heating rates derived from satellite observations.

To download the CloudSat/Calipso heating rates of 2B-FLXHR-LIDAR via sftp, first follow the instructions at
[https://www.cloudsat.cira.colostate.edu/order/sftp-access](https://www.cloudsat.cira.colostate.edu/order/sftp-access) to upload your ssh key and the IP address of the machine that you use for download. I found that to download the data using IMG Jet, I need to use SRVX1 as a jump server (maybe because of some UNIVIE-IMG firewall?):

``sftp -r -i ~/.ssh/id_rsa_cloudsat -J avoigt@srvx1.img.univie.ac.at aiko.voigtATunivie.ac.at@www.cloudsat.cira.colostate.edu:Data/2B-FLXHR-LIDAR.P2_R05/* .``

``~/.ssh/id_rsa_cloudsat`` is the private key that I generated on SRVX1 and made available on both SRVX1 and Jet (this implies that I do not need to provide my password for SRVX1 but the passphrase of the key). Note that I submitted both the SRVX1 and the Jet ssh-keys and IP addresses
to the Cloudsat website, though I tend to think that only the IP address of SRVX1 is needed.


Information on the dataset is available
at [https://www.cloudsat.cira.colostate.edu/data-products/2b-flxhr-lidar](https://www.cloudsat.cira.colostate.edu/data-products/2b-flxhr-lidar).

To read in and work with the HDF4 files, it is helpful to create a dedicated python environment. On Jet, this can be done using micromamba. See the directory ``pythonenv``.

**Directory structure**

* ethz: plots of cloud-radiative heating for a tropical cyclone
* pythonenv: create dedicated python environment
* core: functions to read-in data and compute heating rates per granule
* solar: functions to derive daily-mean insolation, adapted from climlab of Brian Rose (see the readme file in the solar directory)
* papavasileiou2018: crh computed from 2b-flxhr-lidar r04 in Papavasileiou et al., 2018, https://rmets.onlinelibrary.wiley.com/doi/10.1002/qj.3768; downloaded from https://zenodo.org/records/7236564/files/domain-mean_data.zip (--> cloudsat_calipso_global_3d_acre_multiyear_clim.nc)

To generate the binned heating rates, run ``make_binned_heating.py`` followed by ``postprocess_binned_heating.py``. The first script generates heating rates for each year, the second merges the year and takes care of the time and vertical axes, plus adds some metadata.

To validate the code, I also computed CRH for R04. To this end, the Cloudsat data center was so kind to provide the R04 files for download. The analysis scripts mirror those of R05, and are named ``make_binned_heating_R04.py`` and ``postprocess_binned_heating_R04.py``.

``how_to_best_compute_radheating.ipynb`` illustrates that heating rates should be diagnosed by using the flux divergence divided by the pressure level thickness - this is the approach implemented in the core function. 

``plot_zonaltimemean_crh.ipynb`` does a time-mean zonal-mean plot of cloud-radiative heating - this is a sanity check. The plots include CRH data from R05 and R04 computed by me, and R04 CRH data from Papavasileiou et al., 2018.

``sbatch_jet_submit.sh`` is a sample script to run the analysis script on one of the IMG Jet nodes using SLURM.

